
const servidorAPI = "http://localhost/dailyreporter/api/v1.0/";


// ------------------------------------------------
// Funciones generales (pa otros scripts)
// ------------------------------------------------

function getCierroSesion(){

    var recurso = servidorAPI+"logOut/" //Recurso para acceder al cicerre de sesion
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
       cierroSesion(datos["datos"]);
    });
  
  }
  
  
  function cierroSesion(datos){
    if(datos){
        console.log(" Borrando sesion ");
        deleteCookie("proyecto3A_usuario");
        deleteCookie("proyecto3A_clave");
  
        //Compruebo si se esta en una zona que requiere autorizacion
        comprobarLogin();
  
    }
  }
  
  
  function peticionCerrarSesion(){
    getCierroSesion();
  }
  
  
  // -------------------------------------------------
  // COOKIES
  
  
  function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
  
    function deleteCookie(name) {
      document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }