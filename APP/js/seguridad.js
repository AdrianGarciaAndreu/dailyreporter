
/**
 * Si no hay una sesion iniciada, se redirecciona a la pagina principal
 */
function comprobarLogin(){
    if(getCookie("proyecto3A_usuario").length<1){
        window.location.replace("./LogIn.html");
    }
}