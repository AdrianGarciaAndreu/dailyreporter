



function retrieveProject(){
    //var params = "";
    
    //var resource = servidorAPI+"projects/"+params; //Recurso para acceder a todas los usuarios

    var resource = servidorAPI+"project/";

    fetch(resource, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        
        //console.log(datos)
        processProjecets(datos["datos"]);
    });

}



/**
 * Procesa los datos de los proyectos y los dispone en pantalla
 * @param {datos a procesar} datos 
 */
function processProjecets(datos){
    console.log(datos)
    var dataToInsert = "No projects yet";


    if(dataToInsert.length>0){
        
        dataToInsert = '';
        for (const project of datos) {
            //console.log(project);

            var img = "imagen.png";
            if(project["image"]!=null){
                img = project["image"];
            }

            dataToInsert += `<div class="card p-1 my-2 mx-3" style="width: 18rem; align-items:center;">`;
            dataToInsert += `<img src="img/${img}" class="card-img-top p-3" style="width:256px;height:256px;>`;
            dataToInsert += `<div class="card-body">`;
            dataToInsert += `<h5 class="card-title">${project["name"]}</h5>`;
            dataToInsert += `<p class="card-text">${project["description"]}</p>`;
            dataToInsert += `<a href="sprintPanel.html?idProject=${project["id"]}" class="btn btn-primary">Access to sprint view</a>`;
            dataToInsert += `</div">`;
            dataToInsert += `</div">`;

            
        }

    }

    var cardContainer = document.getElementById("card-layout-container");
    cardContainer.innerHTML = dataToInsert;




}












// Load all existing projects
function getProjects(){
    retrieveProject();

}