<?php

/**
 * Vista de respuesta general a las peticiones de la API 
 * Se permiten todos los origenes para la API, los metodos GET, POST, PUT y DELETE
 * y se procesa la salida en formato JSON
 * @author Adrian Garcia Andreu
 * @version 1.1
 */


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Content-Type: application/json; charset=utf-8");

// Nada especifico al margen de los headers
