<?php

/**
 * Inserta una nuevo usuario en la BBDD
 * @author Adrian Garcia Andreu
 * @version 1.2
 */

 // campos obligatorios
 
 $_correo = $form_params["correo"];
 $_clave = $form_params["clave"];

 $_nodo = $form_params["idNodo"];


// Campos opcionales

$nombres = "correo, clave, fechaRegistro, idNodo";
$valores = "'".$_correo."', '".$_clave."', CURRENT_DATE(), ".$_nodo."";

$_nombre = "NULL";
$_apellidos = "NULL";
$_telefono = "NULL";

if (array_key_exists("telefono", $form_params) && $form_params["telefono"] != "NULL" ){
    $_telefono = $form_params["telefono"];
    
    $nombres .=",telefono";
    $valores.= ", '".$_telefono."'";

}

if (array_key_exists("apellidos", $form_params) && $form_params["apellidos"] != "NULL"){
    $_apellidos = $form_params["apellidos"];

    $nombres .=",apellidos";
    $valores.= ", '".$_apellidos."'";
}
if (array_key_exists("nombre", $form_params) && $form_params["nombre"] != "NULL"){
    $_nombre = $form_params["nombre"];

    $nombres .=",nombre";
    $valores.= ", '".$_nombre."'";
}


$sql = "INSERT INTO usuarios(".$nombres.") ";
$sql .= "VALUES(".$valores.")";


//$sql = "INSERT INTO usuarios(correo, clave, nombre, apellidos, fechaRegistro, idNodo, telefono) ";
//$sql .= "VALUES( '".$_correo."', '".$_clave."', '".$_nombre."', ";
//$sql .= "'".$_apellidos."', CURRENT_DATE(), ".$_nodo.", '".$_telefono."')";

//echo $sql;


$respuesta = mysqli_query($conexion, $sql);
