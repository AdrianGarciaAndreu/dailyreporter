<?php
/**
 * Obtiene todas las medidas de la BBDD
 * @author Adrian García
 * @version 1.1
 */

$sql = "SELECT * FROM users";
$resultado = mysqli_query($conexion, $sql);

// Almacena la respuesta en un array asociativo
$respuesta = array();
while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);
};