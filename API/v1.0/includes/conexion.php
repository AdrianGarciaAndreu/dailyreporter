<?php

/**
 * Conexion con la BBDD del servidor rest
 * @author Adrian Garcia Andreu
 * @version 1.1
 */


//Nombre del servidor, bbdd, usuario y clave para realizar la conexion a la bbd
$bbddServer = "localhost";

// Servidor local
$bbddName = "dailyreporter";
$bbddUser = "root"; $bbddPass = ""; 

//Se realiza la conexion y se establece el charset de los datos en utf-8
$conexion = mysqli_connect($bbddServer,$bbddUser,$bbddPass,$bbddName);
mysqli_query($conexion, "SET NAMES utf8");



?>